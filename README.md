# Eureka

## What is Eureka
This simple project attempts to create 4 instances of eureka servers and hook them up in peer to peer mode. 

## Eureka Distros 
* `eureka-spring` - Spring cloud Eureka
* `eureka-netflix` - The original Eureka by Netflix

## Building 
#### Spring Cloud Eureka
Use gradle to build the jar.

```
gradle clean build
```

This should create a jar at `build/libs/discovery-server.jar`

#### Netflix Eureka
The docker file is at `docker/netflix/Dockerfile`
To build the docker image
```
docker build -t alvin2439/eureka-netflix .
``` 

#### Helper Script
Run this script `scripts/build-and-deploy.sh`.

This will build Spring Cloud Eureka jar and copy the jar to `docker/spring/jars/app.jar`

This will also build the Netflix Eureka docker image 
 
## Starting
To start, simply run the following command
```
docker-compose up -d
```

or 

execute the helper script `scripts/start.sh`

## What to expect
#### Running them in peer to peer mode

To do that, we need to set the defaultZoneUrl.
The value to set is 
```
http://172.28.1.11:8080/eureka/v2/,http://172.28.1.12:8080/eureka/v2/,http://172.28.1.13:8080/eureka/,http://172.28.1.14:8080/eureka/
```
For netflix eureka, the API endpoint is at `/eureka/v2` while teh spring cloud version is at `/eureka`.

#### Register using IP instead of Hostname
For Spring Cloud Eureka, this can be achieve by setting the property `eureka.instance.prefer-ip-address` to `true`.

For Netflix Eureka, there doesn't seemed to have any option to do that.
To workaround that, the easiest way is to set the Hostname = IP Address.
In docker-compose, we can achieve that by setting the `hostname` property to the IP Address.

 
  








