package com.battletech.maddog.discovery

import groovy.util.logging.Slf4j
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

import javax.annotation.PostConstruct
import java.time.Instant

@SpringBootApplication(scanBasePackages = ["com.battletech.maddog.discovery"])
@EnableEurekaServer
@Slf4j
class DiscoveryServerApp {

    @PostConstruct
    void init() {
        log.info("Setting system timezone to UTC...")
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
        log.info("Current system timezone is now ${TimeZone.default}")
        log.info("Current system time is ${Instant.now()}")
    }

    static void main(String[] args) {
        SpringApplication.run DiscoveryServerApp, args
    }
}
