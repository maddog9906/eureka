#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}

echo "WORKSPACE is: $WORKSPACE"

cd $WORKSPACE
./gradlew clean build
yes | cp build/libs/discovery-server.jar docker/spring/jars/app.jar
rm -rf build

cd $WORKSPACE/docker/netflix
docker build -f Dockerfile -t alvin2439/eureka-netflix .