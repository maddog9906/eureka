#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"
export WORKSPACE=${WORKSPACE:-${PROJECT_DIR}}
export DOCKER_COMPOSE="docker-compose --log-level ERROR"
export COMPOSE_FILE="${WORKSPACE}/docker/docker-compose.yml"

echo "WORKSPACE is: $WORKSPACE"

${DOCKER_COMPOSE} -f ${COMPOSE_FILE} up -d || exit 1

${PROJECT_DIR}/scripts/./wait-for-it.sh http://172.28.1.11:8080/eureka
${PROJECT_DIR}/scripts/./wait-for-it.sh http://172.28.1.12:8080/eureka
${PROJECT_DIR}/scripts/./wait-for-it.sh http://172.28.1.13:8080/dashboard
${PROJECT_DIR}/scripts/./wait-for-it.sh http://172.28.1.14:8080/dashboard