#!/usr/bin/env bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )"

cd ${PROJECT_DIR}/docker
SERVICE_URL=$1 docker-compose --log-level ERROR up dockerize